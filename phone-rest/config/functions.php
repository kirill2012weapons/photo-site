<?php

function d($var,$caller=null) {
    echo '<pre>';
    yii\helpers\VarDumper::dump($var, 10, true);
    echo '</pre>';
}
function dd($var) {
    d($var);
    die();
}