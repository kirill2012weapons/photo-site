<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 10.05.2018
 * Time: 20:51
 */

namespace app\controllers;


use app\models\jsonCode;
use app\models\PhoneModel;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class PhonesController extends Controller {

    public function actionAddPhone() {

        $_request = array();

//        $_request = [
//            'phone_data' => [
//                'user_id' => 1,
//                'phone' => '99999999999',
//            ],
//        ];

        $_request = \Yii::$app->request->getBodyParams();

        $newPhone = new PhoneModel();

        $newPhone->attributes = $_request['phone_data'];
        $newPhone->user_id = $_request['phone_data']['user_id'];

        if ($newPhone->validate()) {
            if ($newPhone->save()) {
                $response = PhoneModel::find()
                    ->where(['id' => $newPhone->id])
                    ->asArray()
                    ->one();
                return jsonCode::set($response, '');
            }
        } else return jsonCode::set('', $newPhone->errors);

    }

    public function actionDeletePhone() {

        $_request = array();
//
//        $_request = [
//            'phone_id' => 5
//        ];

        $_request = \Yii::$app->request->getBodyParams();

        $phone = PhoneModel::find()
            ->where(['id' => $_request['phone_id']])
            ->one();

        if (!empty($phone)) {

            if ($phone->delete()) {

                return jsonCode::set(['Phone' => 'Phone is deleted.'],'');

            } else return jsonCode::set('',['Phone' =>'Cannot delete phone']);

        } else return jsonCode::set('',['Phone' =>'No Phone Number']);

    }

    public function actionUpdatePhone() {

        $_request = array();

//        $_request = [
//            'phone_id' => 5,
//            'phone_data' => [
//                'phone' => '6666666666',
//            ]
//        ];

        $_request = \Yii::$app->request->getBodyParams();

        $phone = PhoneModel::find()
            ->where(['id' => $_request['phone_id']])
            ->one();

        if (!empty($phone)) {

            $phone->attributes = $_request['phone_data'];

            if ($phone->validate()) {

                if ($phone->save()) {

                    $newPhone = PhoneModel::find()
                        ->where(['id' => $phone->id])
                        ->asArray()
                        ->one();

                    return jsonCode::set($newPhone, '');

                } else return jsonCode::set('',[$phone->errors]);

            } else return jsonCode::set('',$phone->errors);

        } else return jsonCode::set('',['No Phone Number']);

    }

    public function beforeAction($action) {
        if (\Yii::$app->request->isAjax) {
            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        } else {
            $this->asJson(jsonCode::set('',['No Ajax']));
            return false;
        }
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ]

        ];

        return $behaviors;
    }

}