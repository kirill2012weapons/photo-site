<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 10.05.2018
 * Time: 20:09
 */

namespace app\controllers;


use app\models\jsonCode;
use app\models\UserModel;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class UsersController extends Controller {

    public function actionUpdateUser() {

        $_request = array();

//        $_request = [
//            'id' => 1,
//            'data' => [
//                'name' => 'Sashaqweqwe',
//                'surname' => 'qqqqqq',
//                'patronymic' => 'asaassdasdasd',
//            ]
//        ];


        $_request = \Yii::$app->request->getBodyParams();

        $user = UserModel::find()
            ->where(['id' => $_request['id']])
            ->one();

        $user->attributes = $_request['data'];

        if ($user->validate()) {

            if ($user->save()) {

                $updatedUser = UserModel::find()
                    ->where(['users.id' => $user->id])
                    ->joinWith('phones')
                    ->asArray()
                    ->one();

                return jsonCode::set($updatedUser, '');

            } else return jsonCode::set('',['Update User was failed.']);

        } else return jsonCode::set('',$user->errors);

    }

    public function actionDeleteUser() {

        $_request = array();

//        $_request = [
//            'id' => 1,
//        ];

        $_request = \Yii::$app->request->getBodyParams();

        $deleteUser = UserModel::find()
            ->where(['id' => $_request['id']])
            ->one();

        if ($deleteUser->delete()) {

            return jsonCode::set(['User is deleted'],'');

        } else return jsonCode::set('',['Cannot delete User']);

    }

    public function beforeAction($action) {
        if (\Yii::$app->request->isAjax) {
            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        } else {
            $this->asJson(jsonCode::set('',['No Ajax']));
            return false;
        }
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ]

        ];

        return $behaviors;
    }

}