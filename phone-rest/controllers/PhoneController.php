<?php

namespace app\controllers;

use app\models\jsonCode;
use app\models\PhoneModel;
use app\models\UserModel;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use yii\web\Response;

class PhoneController extends Controller {

    public function actionAddRecord() {

        $_request = array();

        $_request = \Yii::$app->request->getBodyParams();

        $_response = array();

        $newUser = new UserModel();
        $newUser->attributes = $_request['userData'];

        $newPhone = new PhoneModel();

        $i = 0;
        foreach ($_request['phonesData']['phones'] as $key) {
            $i++;
            $newPhone->phone = $key;

            $newPhone->validate();
            if ($newPhone->hasErrors()) $_errors['errors'][] = $newPhone->errors;

            $newPhone->refresh();

        }

        $newUser->validate();
        if ($newUser->hasErrors()) $_errors['errors'][] = $newUser->errors;

        if (empty($_errors['errors'])) {

            if ($newUser->save()) {

                foreach ($_request['phonesData']['phones'] as $key) {

                    $newPhone = new PhoneModel();

                    $newPhone->phone = $key;
                    $newPhone->link('users', $newUser);
                    $newPhone->save();

                }

                $savedUser = UserModel::find()
                    ->where(['users.id' => $newUser->id])
                    ->joinWith('phones')
                    ->asArray()
                    ->one();

                return jsonCode::set($savedUser,'');

            } else return jsonCode::set('',$newUser->errors);

        } else return jsonCode::set('',$_errors['errors']);

    }

    public function actionGetRecords() {

        $_request = array();

        $_request = \Yii::$app->request->getBodyParams();

        $_response = array();


        $_response = UserModel::find()
            ->joinWith('phones')
            ->limit($_request['limit'])
            ->offset($_request['start'])
            ->groupBy('users.id')
            ->orderBy([
                'id' => SORT_DESC,
            ])
            ->asArray()
            ->all();

        return jsonCode::set($_response,'');
    }

    public function beforeAction($action) {
        if (\Yii::$app->request->isAjax) {
            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        } else {
            $this->asJson(jsonCode::set('',['No Ajax']));
            return false;
        }
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ]

        ];

        return $behaviors;
    }

}