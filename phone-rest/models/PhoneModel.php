<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 10.05.2018
 * Time: 12:31
 */

namespace app\models;

use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class PhoneModel extends ActiveRecord {

    public function behaviors() {

        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];

    }

    public static function tableName() {

        return '{{phones}}';

    }

    public function rules() {

        return [
            [['phone'], 'required', 'message' => 'Phone must be req.'],
            [['phone'], 'string', 'max' => 20, 'message' => 'Phone length must be less then 20 characters' ],
            [['phone'], 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'Enter only Numbers'],
        ];

    }

    public function getUsers() {

        return $this->hasOne(UserModel::className(), ['id' => 'user_id']);

    }

}