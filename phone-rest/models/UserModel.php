<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 10.05.2018
 * Time: 12:27
 */

namespace app\models;


use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;


class UserModel extends ActiveRecord {

    public function behaviors() {

        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];

    }

    public function rules() {

        return [
            [['name'], 'required', 'message' => 'Name must be req.'],
            [['name', 'surname', 'patronymic'], 'string', 'max' => 100, 'message' => 'Inputs length must be less then 100 characters' ],
            [['surname', 'patronymic'], 'default', 'value' => null],
            [['name', 'surname', 'patronymic'], 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Enter only Char'],
        ];

    }

    public static function tableName() {

        return '{{users}}';

    }

    public function getPhones() {

        return $this->hasMany(PhoneModel::className(), ['user_id' => 'id']);

    }

}