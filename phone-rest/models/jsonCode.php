<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 24.04.2018
 * Time: 14:51
 */

namespace app\models;


use yii\base\BaseObject;

class jsonCode extends BaseObject
{

    static function set($data, $errors) {

        $_mass = Array();

        $_mass['code'] = 200;

        if (empty($errors)) {
            $_mass['success'] = true;
        } else {
            $_mass['success'] = false;
        }

        $_mass['data'] = $data;

        $_mass['errors'] = $errors;

        return $_mass;

    }

    static function setJson($data, $errors) {

        $_mass = Array();

        $_mass['code'] = 200;

        if (empty($errors)) {
            $_mass['success'] = true;
        } else {
            $_mass['success'] = false;
        }

        $_mass['data'] = $data;

        $_mass['errors'] = $errors;

        return json_encode($_mass);

    }

}