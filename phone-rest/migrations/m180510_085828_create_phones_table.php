<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phones`.
 */
class m180510_085828_create_phones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('phones', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'phone' => $this->string(20),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-phones-user_id',
            'phones',
            'user_id'
        );

        $this->addForeignKey(
          'fk-phones-user_id',
          'phones',
          'user_id',
          'users',
          'id',
          'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk-phones-user_id',
            'phones'
        );

        $this->dropIndex(
            'idx-phones-user_id',
            'phones'
        );

        $this->dropTable('phones');
    }
}
