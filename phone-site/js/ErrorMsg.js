$( window ).ready(function() {

window.ErrorMsg = new ErrorMsg();

});

function ErrorMsg() {

    var _ = {};

    var doc = document;

    var wrapper = doc.querySelector('.wrapper');

    _.deleteError = function () {

        var err = doc.querySelector('[data-error-block]');
        if (err) err.remove();

    };

    _.createError = function (dataError) {

        _.deleteError();

        if (dataError.length === 0) return;

        var errBlock = doc.createElement('div');
        errBlock.classList.add('popup-errors');
        errBlock.setAttribute('data-error-block', '');

        var btnClose = doc.createElement('button');
        btnClose.classList.add('close');
        btnClose.setAttribute('data-error-block-btn-close', '');
        btnClose.innerHTML = 'close';
        errBlock.appendChild(btnClose);

        var h3 = doc.createElement('h3');
        h3.innerHTML = 'ERRORS';
        errBlock.appendChild(h3);

        var ul = doc.createElement('ul');

        if (is.array(dataError)) {
            for (var i = 0; i < dataError.length; i++) {

                var obj = dataError[i];

                if (is.object(obj)) {
                    for (var key in obj) {

                        var li = doc.createElement('li');
                        li.innerHTML = key + ' : ' + obj[key];

                        ul.appendChild(li);

                    }
                }

            }
        } else if (is.object(dataError)) {

            var obj = dataError;
            for (var key in obj) {

                if (is.string(obj[key])) {

                    var li = doc.createElement('li');
                    li.innerHTML = key + ' : ' + obj[key];

                    ul.appendChild(li);

                } else if (is.array(obj[key])) {

                    for (var i = 0; i < obj[key].length; i++) {

                        var li = doc.createElement('li');
                        li.innerHTML = obj[key][i];

                        ul.appendChild(li);

                    }

                }

            }
        }

        errBlock.appendChild(ul);

        wrapper.appendChild(errBlock);

    };

    document.addEventListener('click', function (event) {

        if (event.target.hasAttribute('data-error-block-btn-close')) {

            _.deleteError();

        }

    });

    return _;

}