$( window ).ready(function() {

    PhoneController.getRecords();

});

window.PhoneController = new PhoneController();

function PhoneController() {

    var _ = {};

    var doc = document;



    _.startQ = 0;
    _.requestDATA = 10;

    _.requestDATA = {
        'start':_.startQ,
        'limit':50
    };

    _.renderOne = function (dataObj) {

        var divDataUser = doc.createElement('div');
        divDataUser.setAttribute('data-user', '');
        divDataUser.setAttribute('data-user-id', dataObj.id);
        divDataUser.classList.add('user-container');


        var divDataUser_dataUserInformations = doc.createElement('div');
        divDataUser_dataUserInformations.setAttribute('data-user-informations', '');

        var divDataUser_dataUserInformations_ul = doc.createElement('ul');
        var divDataUser_dataUserInformations_ul_liName = doc.createElement('li');
        divDataUser_dataUserInformations_ul_liName.setAttribute('data-user-name', dataObj.name);
        divDataUser_dataUserInformations_ul_liName.innerHTML = dataObj.name;
        divDataUser_dataUserInformations_ul.appendChild(divDataUser_dataUserInformations_ul_liName);
        var divDataUser_dataUserInformations_ul_liSurname = doc.createElement('li');
        divDataUser_dataUserInformations_ul_liSurname.setAttribute('data-user-surname', dataObj.surname);
        divDataUser_dataUserInformations_ul_liSurname.innerHTML = dataObj.surname;
        divDataUser_dataUserInformations_ul.appendChild(divDataUser_dataUserInformations_ul_liSurname);
        var divDataUser_dataUserInformations_ul_liPatronymic = doc.createElement('li');
        divDataUser_dataUserInformations_ul_liPatronymic.setAttribute('data-user-patronymic', dataObj.patronymic);
        divDataUser_dataUserInformations_ul_liPatronymic.innerHTML = dataObj.patronymic;
        divDataUser_dataUserInformations_ul.appendChild(divDataUser_dataUserInformations_ul_liPatronymic);
        var divDataUser_dataUserInformations_ul_liDate = doc.createElement('li');
        divDataUser_dataUserInformations_ul_liDate.setAttribute('data-user-created-at', dataObj.created_at);
        divDataUser_dataUserInformations_ul_liDate.innerHTML = dataObj.created_at;
        divDataUser_dataUserInformations_ul.appendChild(divDataUser_dataUserInformations_ul_liDate);
        divDataUser_dataUserInformations.appendChild(divDataUser_dataUserInformations_ul);

        var buttons = doc.createElement('div');
        buttons.classList.add('buttons');

        var buttons_update = doc.createElement('button');
        buttons_update.classList.add('button');
        buttons_update.setAttribute('data-update-user','');
        buttons_update.innerHTML = 'Update User';
        buttons.appendChild(buttons_update);

        var buttons_delete = doc.createElement('button');
        buttons_delete.classList.add('button');
        buttons_delete.setAttribute('data-delete-user','');
        buttons_delete.innerHTML = 'Delete User';
        buttons.appendChild(buttons_delete);

        var buttons_phoneTrigg = doc.createElement('button');
        buttons_phoneTrigg.classList.add('button');
        buttons_phoneTrigg.classList.add('phone-trigg');
        buttons_phoneTrigg.setAttribute('data-show-phones','');
        buttons_phoneTrigg.innerHTML = 'Show\\Hide Phones';
        buttons.appendChild(buttons_phoneTrigg);

        divDataUser_dataUserInformations.appendChild(buttons);



        var dataUserPhones = doc.createElement('div');
        dataUserPhones.setAttribute('data-user-phones', '');
        dataUserPhones.classList.add('phone-wrapper');
        dataUserPhones.classList.add('disabled');

        var dataUserPhones_ul = doc.createElement('ul');

        for (var i = 0; i < dataObj.phones.length; i++) {

            var li = doc.createElement('li');
            li.setAttribute('data-phone-id', dataObj.phones[i].id);
            li.setAttribute('data-phone', dataObj.phones[i].phone);
            li.innerHTML = dataObj.phones[i].phone;

            var buttons_update_phone = doc.createElement('button');
            buttons_update_phone.classList.add('button');
            buttons_update_phone.classList.add('phone');
            buttons_update_phone.setAttribute('data-update-phone','');
            buttons_update_phone.innerHTML = 'Update';
            li.appendChild(buttons_update_phone);

            var buttons_update_phone = doc.createElement('button');
            buttons_update_phone.classList.add('button');
            buttons_update_phone.classList.add('phone');
            buttons_update_phone.setAttribute('data-delete-phone','');
            buttons_update_phone.innerHTML = 'Delete';
            li.appendChild(buttons_update_phone);

            dataUserPhones_ul.appendChild(li);

        }

        dataUserPhones.appendChild(dataUserPhones_ul);

        var buttonCreatePhone = doc.createElement('button');
        buttonCreatePhone.classList.add('button');
        buttonCreatePhone.classList.add('add-new-phone');
        buttonCreatePhone.setAttribute('data-add-phone','');
        buttonCreatePhone.innerHTML = 'Add Phone Number';

        dataUserPhones.appendChild(buttonCreatePhone);

        divDataUser.appendChild(divDataUser_dataUserInformations);
        divDataUser.appendChild(dataUserPhones);

        return divDataUser;

    };

    _.renderAll = function (data) {

        var dataAllUsers = document.querySelector('[data-all-users]');
        if (!dataAllUsers) return;

        for (var i = 0; i < data.length; i++) {

            dataAllUsers.appendChild(_.renderOne(data[i]));

        }

    };

    _.getRecords = function () {

        var jsonString = JSON.stringify(this.requestDATA);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/get/records",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                console.dir(data);
                if (data.success === true){
                    _.renderAll(data.data);
                } else {
                    ErrorMsg.createError(data.errors);
                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    return _;

};