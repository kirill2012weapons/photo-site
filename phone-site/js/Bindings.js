$( window ).ready(function() {

    $(document)
        .ajaxStart(function(){
            $("#ajaxSpinnerContainer").show();
        })
        .ajaxStop(function(){
            $("#ajaxSpinnerContainer").hide();
        });

});

window.Bindings = new Bindings();

function Bindings() {

    var _ = {};

    var _phones_block = null;

    var doc = document;

    _.PhoneDisable = function () {

        _phones_block = (event.target.closest('[data-user]')).querySelector('[data-user-phones]');

        if (!_phones_block) return;

        if (_phones_block.classList.contains('disabled')) _phones_block.classList.remove('disabled');
        else  _phones_block.classList.add('disabled');

    };

    _.sendUpdatedUserData = function(UserSelector) {

        var request = {};
        request.id = UserSelector.getAttribute('data-user-id');
        request.data = {};
        request.data.name = UserSelector.querySelector('input[data-name-input]').value;
        request.data.surname = UserSelector.querySelector('input[data-surname-input]').value;
        request.data.patronymic = UserSelector.querySelector('input[data-patronymic-input]').value;

        console.dir(request);

        var jsonString = JSON.stringify(request);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/update/user",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                console.dir(data);
                if (data.success === true) {
                    document.querySelector('[data-all-users]').insertBefore( PhoneController.renderOne(data.data) ,UserSelector);
                    UserSelector.remove();
                } else {

                    ErrorMsg.createError(data.errors);

                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    _.sendDeleteUser = function (UserSelector) {

        var request = {};
        request.id = UserSelector.getAttribute('data-user-id');

        var jsonString = JSON.stringify(request);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/delete/user",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                if (data.success == true) {
                    UserSelector.remove();
                } else {

                    ErrorMsg.createError(data.errors);

                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    _.deleteUser = function (UserSelector) {

        if (doc.querySelector('[data-update-block]')) {
            doc.querySelector('[data-update-block]').remove();
        }

        if (UserSelector.querySelector('[data-delete-block]')) {
            doc.querySelector('[data-delete-block]').remove();
            return;
        }

        if (doc.querySelector('[data-delete-block]')) {
            doc.querySelector('[data-delete-block]').remove();
        }

        var dataUpdateBlock = doc.createElement('div');
        dataUpdateBlock.setAttribute('data-delete-block','');
        dataUpdateBlock.setAttribute('data-info-popup','');

        var buttonOK = doc.createElement('button');
        buttonOK.classList.add('button');
        buttonOK.setAttribute('data-delete-user-submit','ok');
        buttonOK.innerHTML = 'OK';

        var buttonNo = doc.createElement('button');
        buttonNo.classList.add('button');
        buttonNo.setAttribute('data-delete-user-submit','no');
        buttonNo.innerHTML = 'NO';

        dataUpdateBlock.appendChild(buttonOK);
        dataUpdateBlock.appendChild(buttonNo);

        var dataIserInformations = UserSelector.querySelector('[data-user-informations]');
        var ulInfo = UserSelector.querySelector('[data-user-informations] .buttons');

        dataIserInformations.insertBefore(dataUpdateBlock, ulInfo);

    };

    _.updateUser = function (UserSelector) {

        if (doc.querySelector('[data-delete-block]')) {
            doc.querySelector('[data-delete-block]').remove();
        }

        if (UserSelector.querySelector('[data-update-block]')) {
            doc.querySelector('[data-update-block]').remove();
            return;
        }

        if (doc.querySelector('[data-update-block]')) {
            doc.querySelector('[data-update-block]').remove();
        }

        var dataUpdateBlock = doc.createElement('div');
        dataUpdateBlock.setAttribute('data-update-block','');
        dataUpdateBlock.setAttribute('data-info-popup','');
        dataUpdateBlock.classList.add('update');

        var dataUpdateBlock_div1 = doc.createElement('div');
        var dataUpdateBlock_div1_span = doc.createElement('span');
        dataUpdateBlock_div1_span.innerHTML = 'Name:';
        dataUpdateBlock_div1.appendChild(dataUpdateBlock_div1_span);
        var dataUpdateBlock_div_input = doc.createElement('input');
        dataUpdateBlock_div_input.setAttribute('data-name-input', '');
        dataUpdateBlock_div_input.setAttribute('value', UserSelector.querySelector('[data-user-name]').getAttribute('data-user-name'));
        dataUpdateBlock_div1.appendChild(dataUpdateBlock_div_input);

        var dataUpdateBlock_div2 = doc.createElement('div');
        var dataUpdateBlock_div2_span = doc.createElement('span');
        dataUpdateBlock_div2_span.innerHTML = 'Surname:';
        dataUpdateBlock_div2.appendChild(dataUpdateBlock_div2_span);
        var dataUpdateBlock_div_input = doc.createElement('input');
        dataUpdateBlock_div_input.setAttribute('data-surname-input', '');
        dataUpdateBlock_div_input.setAttribute('value', UserSelector.querySelector('[data-user-surname]').getAttribute('data-user-surname'));
        dataUpdateBlock_div2.appendChild(dataUpdateBlock_div_input);

        var dataUpdateBlock_div3 = doc.createElement('div');
        var dataUpdateBlock_div3_span = doc.createElement('span');
        dataUpdateBlock_div3_span.innerHTML = 'Patronymic:';
        dataUpdateBlock_div3.appendChild(dataUpdateBlock_div3_span);
        var dataUpdateBlock_div_input = doc.createElement('input');
        dataUpdateBlock_div_input.setAttribute('data-patronymic-input', '');
        dataUpdateBlock_div_input.setAttribute('value', UserSelector.querySelector('[data-user-patronymic]').getAttribute('data-user-patronymic'));
        dataUpdateBlock_div3.appendChild(dataUpdateBlock_div_input);

        var buttonOK = doc.createElement('button');
        buttonOK.classList.add('button');
        buttonOK.setAttribute('data-update-user-submit','');
        buttonOK.innerHTML = 'OK';

        dataUpdateBlock.appendChild(dataUpdateBlock_div1);
        dataUpdateBlock.appendChild(dataUpdateBlock_div2);
        dataUpdateBlock.appendChild(dataUpdateBlock_div3);
        dataUpdateBlock.appendChild(buttonOK);

        var dataIserInformations = UserSelector.querySelector('[data-user-informations]');
        var ulInfo = UserSelector.querySelector('[data-user-informations] .buttons');

        dataIserInformations.insertBefore(dataUpdateBlock, ulInfo);

    };

    _.addPhone = function (UserSelector) {

        var dataAddPhone = doc.createElement('div');
        dataAddPhone.setAttribute('data-add-phone-new', '');
        dataAddPhone.setAttribute('data-info-popup', '');

        var div_span = doc.createElement('span');
        div_span.innerHTML = 'Phone: ';
        dataAddPhone.appendChild(div_span);

        var div_input = doc.createElement('input');
        div_input.setAttribute('data-add-phone-new-input', '');
        div_input.setAttribute('value', '');
        dataAddPhone.appendChild(div_input);

        var btnSub = doc.createElement('button');
        btnSub.classList.add('button');
        btnSub.setAttribute('data-add-phone-new-submit', 'ok');
        btnSub.innerHTML = 'OK';
        dataAddPhone.appendChild(btnSub);

        var ulInfo = UserSelector.querySelector('[data-user-phones]');

        ulInfo.appendChild(dataAddPhone);

        $('[data-add-phone-new-input]').mask('(000) 00-00-0000');

    };

    _.renderNewPhone = function (data) {

        var li = doc.createElement('li');
        li.setAttribute('data-phone-id', data.id);
        li.setAttribute('data-phone', data.phone);
        li.innerHTML = data.phone;

        var buttons_update_phone = doc.createElement('button');
        buttons_update_phone.classList.add('button');
        buttons_update_phone.classList.add('phone');
        buttons_update_phone.setAttribute('data-update-phone','');
        buttons_update_phone.innerHTML = 'Update';
        li.appendChild(buttons_update_phone);

        var buttons_update_phone = doc.createElement('button');
        buttons_update_phone.classList.add('button');
        buttons_update_phone.classList.add('phone');
        buttons_update_phone.setAttribute('data-delete-phone','');
        buttons_update_phone.innerHTML = 'Delete';
        li.appendChild(buttons_update_phone);

        return li;

    };

    _.sendNewPhone = function (UserSelector) {

//        $_request = [
//            'phone_data' => [
//                'user_id' => 1,
//                'phone' => '99999999999',
//            ],
//        ];

        var request = {};
        request.phone_data = {};
        request.phone_data.user_id = UserSelector.getAttribute('data-user-id');
        request.phone_data.phone = $('[data-add-phone-new-input]').cleanVal();

        var jsonString = JSON.stringify(request);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/add/phone",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                if (data.success === true) {

                    UserSelector.querySelector('[data-user-phones] ul').appendChild(_.renderNewPhone(data.data));

                    UserSelector.querySelector('[data-add-phone-new]').remove();
                } else {

                    console.dir(data);
                    ErrorMsg.createError(data.errors);

                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    _.sendUpdatePhone = function (UserSelector, PhoneSelector) {

        var request = {};
        request.phone_id = PhoneSelector.getAttribute('data-phone-id');
        request.phone_data = {};
        request.phone_data.phone = $('[data-update-phone-input]').cleanVal();

        var jsonString = JSON.stringify(request);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/update/phone",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                if (data.success === true) {

                    PhoneSelector.querySelector('[data-update-phone-block]').remove();
                    PhoneSelector.setAttribute('data-phone', data.data.phone);
                    PhoneSelector.innerText = data.data.phone;

                    var buttons_update_phone = doc.createElement('button');
                    buttons_update_phone.classList.add('button');
                    buttons_update_phone.classList.add('phone');
                    buttons_update_phone.setAttribute('data-update-phone','');
                    buttons_update_phone.innerHTML = 'Update';
                    PhoneSelector.appendChild(buttons_update_phone);

                    var buttons_update_phone = doc.createElement('button');
                    buttons_update_phone.classList.add('button');
                    buttons_update_phone.classList.add('phone');
                    buttons_update_phone.setAttribute('data-delete-phone','');
                    buttons_update_phone.innerHTML = 'Delete';
                    PhoneSelector.appendChild(buttons_update_phone);

                } else {

                    ErrorMsg.createError(data.errors);

                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    _.updatePhone = function (UserSelector) {

        var dataUpdatePhoneBlock = doc.createElement('div');
        dataUpdatePhoneBlock.setAttribute('data-update-phone-block', '');
        dataUpdatePhoneBlock.setAttribute('data-info-popup', '');

        var div = doc.createElement('div');
        dataUpdatePhoneBlock.appendChild(div);

        var div_span = doc.createElement('span');
        div_span.innerHTML = 'Phone: ';
        div.appendChild(div_span);

        var div_input = doc.createElement('input');
        div_input.setAttribute('data-update-phone-input', '');
        div_input.setAttribute('value', UserSelector.getAttribute('data-phone'));
        div.appendChild(div_input);

        var btnSub = doc.createElement('button');
        btnSub.classList.add('button');
        btnSub.classList.add('phone');
        btnSub.setAttribute('data-update-phone-submit', '');
        btnSub.innerHTML = 'OK';
        div.appendChild(btnSub);

        var ulInfo = UserSelector.querySelector('[data-update-phone]');

        UserSelector.insertBefore(dataUpdatePhoneBlock, ulInfo);

        $('[data-update-phone-input]').mask('(000) 00-00-0000');

    };

    _.renderPhoneInput_newUser = function () {

        var dataCreatePhoneBlock = doc.querySelector('[data-create-phone-block]');

        if (!dataCreatePhoneBlock) return;

        if (dataCreatePhoneBlock.querySelector('[data-create-phone-new-user]')) {
            dataCreatePhoneBlock.querySelector('[data-create-phone-new-user]').remove();
            return;
        }

        var dataCreatePhone = doc.createElement('div');
        dataCreatePhone.setAttribute('data-create-phone-new-user', '');
        dataCreatePhoneBlock.insertBefore(dataCreatePhone, doc.querySelector('[data-create-phone-block] ul'));

        var div_span = doc.createElement('span');
        div_span.innerHTML = 'Phone: ';
        dataCreatePhone.appendChild(div_span);

        var div_input = doc.createElement('input');
        div_input.setAttribute('data-create-phone-new-input', '');
        div_input.setAttribute('value', '');
        dataCreatePhone.appendChild(div_input);
        $(div_input).mask('(000) 00-00-0000');

        var btnSub = doc.createElement('button');
        btnSub.classList.add('button');
        btnSub.setAttribute('data-create-phone-new-submit', 'ok');
        btnSub.innerHTML = 'OK';
        dataCreatePhone.appendChild(btnSub);



    };

    _.seletePhoneButton = function (UserSelector, PhoneSelector) {

        if (PhoneSelector.querySelector('[data-button-sub-delete-phone]')) {
            PhoneSelector.querySelector('[data-button-sub-delete-phone]').remove();
            return;
        }

        if (UserSelector.querySelector('[data-button-sub-delete-phone]')) UserSelector.querySelector('[data-button-sub-delete-phone]').remove();

        var btnSub = doc.createElement('button');
        btnSub.classList.add('button');
        btnSub.classList.add('phone');
        btnSub.setAttribute('data-button-sub-delete-phone', '');
        btnSub.setAttribute('data-info-popup', '');
        btnSub.innerHTML = 'OK';
        PhoneSelector.appendChild(btnSub);

    };

    _.renderUserForm = function () {

        var dataBlockForNewUser = doc.querySelector('[data-block-for-new-user]');

        var dataCreateRecordBlock = doc.createElement('div');
        dataCreateRecordBlock.setAttribute('data-create-record-block', '');
        dataCreateRecordBlock.classList.add('update');
        dataBlockForNewUser.appendChild(dataCreateRecordBlock);

        var _name_div = doc.createElement('div');
        dataCreateRecordBlock.appendChild(_name_div);
        var _name_div_span = doc.createElement('span');
        _name_div_span.innerHTML = 'Name: ';
        _name_div.appendChild(_name_div_span);
        var _name_div_input = doc.createElement('input');
        _name_div_input.setAttribute('data-name-create-input', '');
        _name_div.appendChild(_name_div_input);

        var _name_div = doc.createElement('div');
        dataCreateRecordBlock.appendChild(_name_div);
        var _name_div_span = doc.createElement('span');
        _name_div_span.innerHTML = 'Surname: ';
        _name_div.appendChild(_name_div_span);
        var _name_div_input = doc.createElement('input');
        _name_div_input.setAttribute('data-surname-create-input', '');
        _name_div.appendChild(_name_div_input);

        var _name_div = doc.createElement('div');
        dataCreateRecordBlock.appendChild(_name_div);
        var _name_div_span = doc.createElement('span');
        _name_div_span.innerHTML = 'Patronymic: ';
        _name_div.appendChild(_name_div_span);
        var _name_div_input = doc.createElement('input');
        _name_div_input.setAttribute('data-patronymic-create-input', '');
        _name_div.appendChild(_name_div_input);

        var dataAddCreatePhoneBtn = doc.createElement('button');
        dataAddCreatePhoneBtn.classList.add('button');
        dataAddCreatePhoneBtn.classList.add('add-new-phone');
        dataAddCreatePhoneBtn.setAttribute('data-add-create-phone', '');
        dataAddCreatePhoneBtn.innerHTML = 'Add Phone Number';
        dataCreateRecordBlock.appendChild(dataAddCreatePhoneBtn);


        var dataCreatePhoneBlock = doc.createElement('div');
        dataCreatePhoneBlock.setAttribute('data-create-phone-block', '');
        dataCreateRecordBlock.appendChild(dataCreatePhoneBlock);

        var dataCreatePhone = doc.createElement('div');
        dataCreatePhone.setAttribute('data-create-phone-new-user', '');
        dataCreatePhoneBlock.appendChild(dataCreatePhone);

        var div_span = doc.createElement('span');
        div_span.innerHTML = 'Phone: ';
        dataCreatePhone.appendChild(div_span);

        var div_input = doc.createElement('input');
        div_input.setAttribute('data-create-phone-new-input', '');
        div_input.setAttribute('value', '');
        dataCreatePhone.appendChild(div_input);
        $(div_input).mask('(000) 00-00-0000');

        var btnSub = doc.createElement('button');
        btnSub.classList.add('button');
        btnSub.setAttribute('data-create-phone-new-submit', 'ok');
        btnSub.innerHTML = 'OK';
        dataCreatePhone.appendChild(btnSub);

        var ul = doc.createElement('ul');
        ul.classList.add('phone-creating');
        dataCreatePhoneBlock.appendChild(ul);

        dataBlockForNewUser.appendChild(dataCreateRecordBlock);

        var btn_Submit = doc.createElement('button');
        btn_Submit.classList.add('btn-new-user');
        btn_Submit.classList.add('button');
        btn_Submit.setAttribute('new-user-sub-btn', '');
        btn_Submit.innerHTML = 'New User Submit';

        dataCreateRecordBlock.appendChild(btn_Submit);

    };

    _.renderNewPhone_NewUser = function () {

        var ul = doc.querySelector('[data-create-phone-block] ul.phone-creating');

        var val = $('[data-create-phone-new-input]').cleanVal();

        if (ul.children.length === 10) return;

        if (val.length === 0) return;

        // $('[data-create-phone-new-input]').clear();

        var li = doc.createElement('li');
        li.setAttribute('new-phones-data', val);
        li.innerHTML = val;

        var span = doc.createElement('span');
        span.innerHTML = 'DELETE';
        span.setAttribute('data-delete-phone-on-new-user','');
        li.appendChild(span);

        ul.appendChild(li);

    };

    _.deletePhoneNewUser = function (PhoneSelector) {

        PhoneSelector.remove();

    };

    _.sendDeletePhone = function (PhoneSelector) {

        var request = {};
        request.phone_id = PhoneSelector.getAttribute('data-phone-id');

        var jsonString = JSON.stringify(request);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/delete/phone",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                if (data.success === true) {

                    PhoneSelector.remove();

                } else {

                    ErrorMsg.createError(data.errors);

                }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    _.sendNewUser = function () {

        var newUser = {};

        var dataCreateRecordBlock = doc.querySelector('[data-create-record-block]');

        if (!dataCreateRecordBlock) return;

        newUser.userData = {};

        newUser.userData.name = '';
        newUser.userData.surname = '';
        newUser.userData.patronymic = '';

        newUser.phonesData = {};
        newUser.phonesData.phones = [];

        newUser.userData.name = dataCreateRecordBlock.querySelector('[data-name-create-input]').value;
        newUser.userData.surname = dataCreateRecordBlock.querySelector('[data-surname-create-input]').value;
        newUser.userData.patronymic = dataCreateRecordBlock.querySelector('[data-patronymic-create-input]').value;

        var _phonesSelector = document.querySelectorAll('[new-phones-data]');

        if (_phonesSelector.length !== 0) {

            for (var i = 0; i < _phonesSelector.length; i++) {

                newUser.phonesData.phones.push( _phonesSelector[i].getAttribute('new-phones-data') );

            }

        } else newUser.phonesData.phones = [];

        var jsonString = JSON.stringify(newUser);

        $.ajax({
            method: "POST",
            data: jsonString,
            url: "http://localhost/phone-site/phone-rest/api/phone/new/record",
            contentType: "application/json; charset=utf-8;",
            success: function (data) {
                    if (data.success) {

                        document.querySelector('[data-create-record-block]').remove();

                        var dataAllUsers = doc.querySelector('[data-all-users]');
                        dataAllUsers.insertBefore(PhoneController.renderOne(data.data), dataAllUsers.children[0]);

                    } else {

                        ErrorMsg.createError(data.errors);

                    }
            },
            error: function (err1, err2) {
                console.dir(err1);
                console.dir(err2);
            }
        });

    };

    document.addEventListener('click', function (event) {

        if (event.target.hasAttribute('data-show-phones')) {

            $('[data-info-popup]').remove();
            _.PhoneDisable();

        } else if (event.target.hasAttribute('data-update-user')) {

            if (event.target.closest('[data-user]').querySelector('[data-update-block]')) {

                event.target.closest('[data-user]').querySelector('[data-update-block]').remove();
                return;

            }

            $('[data-info-popup]').remove();
            _.updateUser(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-update-user-submit')) {

            _.sendUpdatedUserData(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-delete-user')) {

            if (event.target.closest('[data-user]').querySelector('[data-delete-block]')) {

                event.target.closest('[data-user]').querySelector('[data-delete-block]').remove();
                return;

            }

            $('[data-info-popup]').remove();
            _.deleteUser(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-delete-user-submit') && (event.target.getAttribute('data-delete-user-submit') === 'ok')) {

            _.sendDeleteUser(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-delete-user-submit') && (event.target.getAttribute('data-delete-user-submit') === 'no')) {

            $('[data-info-popup]').remove();
            if (doc.querySelector('[data-delete-block]')) doc.querySelector('[data-delete-block]').remove();

        } else if (event.target.hasAttribute('data-update-phone')) {

            if (event.target.closest('[data-user]').querySelector('[data-update-phone-block]')) {

                event.target.closest('[data-user]').querySelector('[data-update-phone-block]').remove();
                return;

            }

            $('[data-info-popup]').remove();
            if (event.target.closest('li').querySelector('[data-update-phone-block]')) {
                event.target.closest('li').querySelector('[data-update-phone-block]').remove();
                return;
            }

            if (document.querySelector('[data-update-phone-block]')) document.querySelector('[data-update-phone-block]').remove();

            _.updatePhone(event.target.closest('li'));

        } else if (event.target.hasAttribute('data-update-phone-submit')) {

            _.sendUpdatePhone(event.target.closest('[data-user]'), event.target.closest('li'));

        } else if (event.target.hasAttribute('data-delete-phone')) {

            if (event.target.closest('[data-user]').querySelector('[data-button-sub-delete-phone]')) {

                event.target.closest('[data-user]').querySelector('[data-button-sub-delete-phone]').remove();
                return;

            }

            $('[data-info-popup]').remove();
            if (event.target.closest('li').querySelector('[data-update-phone-block]')) event.target.closest('li').querySelector('[data-update-phone-block]').remove();

            _.seletePhoneButton(event.target.closest('[data-user]'), event.target.closest('li'));

        } else if (event.target.hasAttribute('data-button-sub-delete-phone')) {

            _.sendDeletePhone(event.target.closest('li'));

        } else if (event.target.hasAttribute('data-add-phone')) {

            if (event.target.closest('[data-user]').querySelector('[data-add-phone-new]')) {

                event.target.closest('[data-user]').querySelector('[data-add-phone-new]').remove();
                return;

            }

            $('[data-info-popup]').remove();
            if (event.target.closest('[data-user]').querySelector('[data-add-phone-new]')) {

                event.target.closest('[data-user]').querySelector('[data-add-phone-new]').remove();
                return;

            }

            _.addPhone(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-add-phone-new-submit')) {

            _.sendNewPhone(event.target.closest('[data-user]'));

        } else if (event.target.hasAttribute('data-add-new-user-button')) {

            $('[data-info-popup]').remove();
            if (document.querySelector('[data-create-record-block]')) {
                document.querySelector('[data-create-record-block]').remove();
                return;
            }
            _.renderUserForm();

        } else if (event.target.hasAttribute('data-create-phone-new-submit')) {

            _.renderNewPhone_NewUser();

        } else if (event.target.hasAttribute('data-delete-phone-on-new-user')) {

            _.deletePhoneNewUser(event.target.closest('[new-phones-data]'));

        } else if (event.target.hasAttribute('data-add-create-phone')) {

            _.renderPhoneInput_newUser();

        } else if (event.target.hasAttribute('new-user-sub-btn')) {

            _.sendNewUser();

        }


    });

    return _;

}
